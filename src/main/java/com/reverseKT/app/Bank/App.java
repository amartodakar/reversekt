package com.reverseKT.app.Bank;

interface Account
{
	public void deposite(double amt);
	public void withdraw(double amt);
	public void bal_enq();
}
class Savings implements Account
{
	double accbal;
	String cname;
	public Savings(double accbal,String cname) 
	{
		this.accbal=accbal;
		this.cname=cname;
	}
	public void deposite(double amt)
	{
		System.out.println("depositing rs:"+amt);
		accbal=accbal+amt;
	}
	public void withdraw(double amt)
	{
		System.out.println("withdraw rs:"+amt);
		accbal=accbal-amt;
	}
	public void bal_enq()
	{
		System.out.println("remaining bal="+accbal);
	}
	
}
class Loan implements Account
{
	double accbal;
	String cname;
	public Loan(double accbal,String cname) 
	{
		this.accbal=accbal;
		this.cname=cname;
	}
	public void deposite(double amt)
	{
		System.out.println("depositing rs:"+amt);
		accbal=accbal-amt;
	}
	public void withdraw(double amt)
	{
		System.out.println("withdraw rs:"+amt);
		accbal=accbal+amt;
	}
	public void bal_enq()
	{
		System.out.println("outstanding bal="+accbal);
	}
}
class AccountManager
{
	static Account createAccount(char acctype, String cname, double initAmt)
	{
		Account acc= null;
		if(acctype == 'S')
		{
			System.out.println("create Savings account");
			acc=new Savings(initAmt,cname);
		}
		else if(acctype == 'L')
		{
			System.out.println("create Loan account");
			acc=new Loan(initAmt,cname);
		}
		return acc;
			
	}
	
}
public class App 
{
    public static void main( String[] args )
    {
        Account a1 = AccountManager.createAccount('L',"Amar",10000);
        a1.bal_enq();
        a1.deposite(5000.00);
        a1.bal_enq();
        a1.withdraw(3000.00);
        a1.bal_enq();
    }
}
